output "ip_web" {
  value       = aws_instance.web.public_ip
  description = "IP address of the web instance"
}

output "ip_other" {
  value       = aws_instance.other.public_ip
  description = "IP address of the other instance"
}
