variable "project" {
  type        = string
  description = "Project Name"
}

variable "owner" {
  type        = string
  description = "Project Owner"
}

variable "tags" {
  type = map(string)
  default = {
    ManagedBy = "Terraform"
  }
}

variable "sg" {
  type = any
}

variable "sub_pvt" {
  type = any
}

variable "sub_pub" {
  type = any
}

variable "environment" {
  type        = string
  description = "Project Environment"
}

variable "ami" {
  type        = string
  default     = "ami-0ad3347eb89bb7702"
  description = "AMI type of VM (default to Debian 10)"
}

variable "key" {
  type    = string
  default = "key-priv"
}

