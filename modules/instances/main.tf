# Create Instances
resource "aws_instance" "web" {
  instance_type = "t2.micro"
  ami           = var.ami
  key_name      = var.key

  vpc_security_group_ids = [
    "${var.sg.sg1}",
  ]
  subnet_id = var.sub_pub[0].id

  tags = merge(
    {
      Name        = "${var.project}-Web"
      Project     = "${var.project}",
      Owner       = "${var.owner}",
      Environment = "${var.environment}",
    },
    var.tags
  )
}

resource "aws_instance" "other" {
  instance_type = "t2.micro"
  ami           = var.ami
  key_name      = var.key

  vpc_security_group_ids = [
    "${var.sg.sg2}",
  ]
  subnet_id = var.sub_pri[0].id

  tags = merge(
    {
      Name        = "${var.project}-Other"
      Project     = "${var.project}",
      Owner       = "${var.owner}",
      Environment = "${var.environment}",
    },
    var.tags
  )
}
