output "sg" {
  value = {
    sg1 = "${aws_security_group.sg1.id}"
    sg2 = "${aws_security_group.sg2.id}"
  }
}

output "vpc" {
  value = aws_vpc.main
}

output "public_subnet_ids" {
  value = aws_subnet.public
}

output "private_subnet_ids" {
  value = aws_subnet.private
}
