variable "project" {
  type        = string
  description = "Project Name"
}

variable "owner" {
  type        = string
  description = "Project Owner"
}

variable "tags" {
  type = map(string)
  default = {
    ManagedBy = "Terraform"
  }
}

variable "environment" {
  type        = string
  description = "Project Environment"
}

variable "cidr_block" {
  type        = string
  default     = "10.0.0.0/16"
  description = "CIDR block for the VPC"
}

variable "public_subnet_cidr_block" {
  type        = list(string)
  default     = ["10.0.2.0/24", "10.0.0.0/24"]
  description = "values for the public subnet cidr blocks"
}

variable "private_subnet_cidr_block" {
  type        = list(string)
  default     = ["10.0.3.0/24", "10.0.1.0/24"]
  description = "values for the private subnet cidr blocks"
}
