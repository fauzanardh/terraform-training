data "aws_availability_zones" "available" {
  state = "available"
}

#  Main VPC
resource "aws_vpc" "main" {
  cidr_block           = var.vpc_cidr
  instance_tenancy     = "default"
  enable_dns_hostnames = true

  tags = merge(
    {
      Name        = "${var.project}-MainVPC"
      Project     = "${var.project}",
      Owner       = "${var.owner}",
      Environment = "${var.environment}",
    },
    var.tags
  )
}

# Public Subnet
resource "aws_subnet" "public" {
  count = length(var.public_subnet_cidr_block)

  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.public_subnet_cidr_block[count.index]
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true

  tags = merge(
    {
      Name        = "${var.project}-PublicSubnet"
      Project     = "${var.project}",
      Owner       = "${var.owner}",
      Environment = "${var.environment}",
    },
    var.tags
  )
}

# Private Subnet
resource "aws_subnet" "private" {
  count = length(var.private_subnet_cidr_block)

  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.private_subnet_cidr_block[count.index]
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = false

  tags = merge(
    {
      Name        = "${var.project}-PrivateSubnet"
      Project     = "${var.project}",
      Owner       = "${var.owner}",
      Environment = "${var.environment}",
    },
    var.tags
  )
}

# Security Group
resource "aws_security_group" "sg1" {
  vpc_id      = aws_vpc.main.id
  name        = "allow_https_ssh_only"
  description = "Allow only SSH and HTTPS"

  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = "443"
    to_port     = "443"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "allow_https_ssh_only"
  }
}

resource "aws_security_group" "sg2" {
  vpc_id      = aws_vpc.main.id
  name        = "allow_ssh_only"
  description = "Allow only SSH"

  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "allow_ssh_only"
  }
}
