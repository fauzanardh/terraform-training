provider "aws" {
  region     = "ap-northeast-1"
  access_key = "aws-access-key"
  secret_key = "aws-secret-key"
}

module "networking" {
  source      = "./modules/networking"
  project     = "Terraform Training"
  owner       = "User1"
  environment = "dev"

  cidr_block                 = "10.0.0.0/16"
  public_subnet_cidr_blocks  = ["10.0.0.0/24", "10.0.2.0/24"]
  private_subnet_cidr_blocks = ["10.0.1.0/24", "10.0.3.0/24"]
}

module "instances" {
  source      = "./modules/instances"
  depends_on  = [module.networking]
  sg          = module.networking.sg
  sub_pvt     = module.networking.private_subnet_ids
  sub_pub     = module.networking.public_subnet_ids
  project     = "Terraform Training"
  owner       = "User1"
  environment = "dev"
}
