output "web" {
  value       = aws_instance.web.public_ip
  description = "Public IP address of the web instance"
}

output "other" {
  value       = aws_instance.other.private_ip
  description = "Private IP address of the other instance"
}
